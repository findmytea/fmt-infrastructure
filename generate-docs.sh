redoc-cli bundle http://localhost:5000/swagger/v1/swagger.json -o nginx/docs/fmt-location-service.html
redoc-cli bundle http://localhost:5002/swagger/v1/swagger.json -o nginx/docs/fmt-brand-service.html
redoc-cli bundle http://localhost:5004/swagger/v1/swagger.json -o nginx/docs/fmt-address-lookup-service.html
redoc-cli bundle http://localhost:5006/swagger/v1/swagger.json -o nginx/docs/fmt-social-media-service.html
redoc-cli bundle http://localhost:5012/swagger/v1/swagger.json -o nginx/docs/fmt-resource-service.html
