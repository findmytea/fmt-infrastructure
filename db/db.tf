
resource "digitalocean_database_cluster" "db" {
  name       = "fmt-mongo-cluster"
  engine     = "mongodb"
  version    = "4"
  size       = "db-s-1vcpu-1gb"
  region     = "lon1"
  node_count = 1
  private_network_uuid = data.digitalocean_vpc.vpc.id
}

# terraform import digitalocean_database_cluster.db 3adff9c1-5873-4c7a-9562-1d83985b8cb3

# terraform state rm digitalocean_database_cluster.db 
