# Configure the DigitalOcean Provider
provider "digitalocean" {
  token   = var.do_token
}

terraform {
  backend "remote" {
    organization = "Paul_Grenyer"

    workspaces {
      name = "fmt-db"
    }
  }
}