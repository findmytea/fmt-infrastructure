### Kuberenetes

doctl auth init

doctl kubernetes cluster kubeconfig save 88988ab5-6ba2-4976-8248-ff64058a8d6f

kubectl --context do-lon1-fmt-k8--context do-lon1-fmt-k8 apply -f <filename>

kubectl port-forward <pod> <port>:<port>

kubectl config get-contexts

kubectl config use-context <cluster-name>

kubectl logs my-pod --previous

kubectl logs -f mq-0 

kubectl --context do-lon1-fmt-k8 get pod mq-8 -o go-template="{{range .status.containerStatuses}}{{.lastState.terminated.message}}{{end}}"

kubectl --context do-lon1-fmt-k8 get event 

kubectl rollout restart deploy



### Helm

https://www.digitalocean.com/community/tutorials/how-to-install-software-on-kubernetes-clusters-with-the-helm-3-package-manager

cd /tmp

curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3

chmod u+x get_helm.sh

./get_helm.sh

### nginx-ingress

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

helm repo update

helm install nginx-ingress ingress-nginx/ingress-nginx --set controller.publishService.enabled=true

kubectl --namespace default get services -o wide -w nginx-ingress-ingress-nginx-controller

/etc/host

188.166.137.21  hw1.your_domain
188.166.137.21  hw2.your_domain


kubectl create namespace cert-manager

helm repo add jetstack https://charts.jetstack.io

helm repo update

helm install cert-manager jetstack/cert-manager --namespace cert-manager --version v1.2.0 --set installCRDs=true

kubectl describe certificate nginx-ingress-tls

### RabbitMQ

helm install rabbit-mq bitnami/rabbitmq-cluster-operator

kubectl get deploy -w --namespace default -l app.kubernetes.io/name=rabbitmq-cluster-operator,app.kubernetes.io/instance=rabbit-mq

kubectl port-forward rabbitmq-0 15672:15672 5672:5672


# Minikube

Minikube start / stop


### install ingress
minikube addons enable ingress 

kubectl get pods -n kube-system


### Metrics

kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
kubectl top pod --sort-by=memory


### Sumologic

https://github.com/SumoLogic/sumologic-kubernetes-collection/blob/main/deploy/docs/Installation_with_Helm.md

helm repo add sumologic https://sumologic.github.io/sumologic-kubernetes-collection

helm upgrade --install sumo sumologic/sumologic --set sumologic.accessId="suqkYdH3TwLYac" --set sumologic.accessKey="" --set sumologic.clusterName="find-my-tea"

helm uninstall sumo

### Unleash

https://fmt-unleash-tea.herokuapp.com

Install Unleash via Herou:

https://docs.getunleash.io/

Use https://uptimerobot.com/ to keep it alive on the free tier.
