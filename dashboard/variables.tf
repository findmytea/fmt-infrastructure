variable "do_token" {
    description = "Digital ocean token"
}

variable "api_key" {
    description = ""
}

variable "jwt_key" {
    description = ""
}

variable "firebase_private_key" {
    description = ""
}

variable "enabled_firebase" {
    default = "false"
}

variable "google_api_key" {
    description = ""
}

variable "ext_api_url" {
    default = "https://api.findmytea.co.uk"
}

variable "user_service_url" {
    default = "https://api.findmytea.co.uk/user"
}

variable "social_media_service_url" {
    default = "https://api.findmytea.co.uk/socialmedia"
}

variable "location_service_url" {
    default = "https://api.findmytea.co.uk/location"
}

variable "establishment_service_url" {
    default = "https://api.findmytea.co.uk/establishment"
}

variable "location_attribute_service_url" {
    default = "https://api.findmytea.co.uk/chain"
}

variable "chain_service_url" {
    default = "https://api.findmytea.co.uk/chain"
}

variable "brand_service_url" {
    default = "https://api.findmytea.co.uk/brand"
}

variable "resource_service_url" {
    default = "https://api.findmytea.co.uk/image"
}

variable "reference_service_url" {
    default = "https://api.findmytea.co.uk"
}

variable "lambda_service_url" {
    default = "https://api.findmytea.co.uk/lambda"
}

variable "address_lookup_service_url" {
    default = "https://api.findmytea.co.uk/addresslookup"
}

variable "messaging_service_url" {
    default = "https://api.findmytea.co.uk"
}

variable "claim_service_url" {
    default = "https://api.findmytea.co.uk"
}

variable "offer_service_url" {
    default = "https://api.findmytea.co.uk"
}

variable "search_service_url" {
    default = "https://api.findmytea.co.uk"
}

variable "geocode_service_url" {
    default = "https://api.findmytea.co.uk/geocode"
}

variable "spaces_endpoint" {
    default = "ams3.digitaloceanspaces.com"
}

variable "spaces_key" {
    default = "2OQGM46UNFGXAXRQWY6I"
}

variable "spaces_secret" {
        description = ""
}

variable "stripe_key" {
    default = "pk_live_51JVFGDKKZXd18PorDyAKxF4RxB8SbQUbA9W9W0pDy1bigpdq9gP0wx3Fxu5MdcX7BnKezD13S3afdl48wow47cIq00JEViUzVP"
}

variable "stripe_secret" {
        description = ""
}

variable "sumo_url" {
        default = "https://endpoint1.collection.eu.sumologic.com/receiver/v1/http/ZaVnC4dhaV2kw-pxmBbHS3sAX3Rv1P6lRQ3FcbX7iysdkIBYyqKTdBae5ZUkYZiKp0ms6Y573hm3bxWaYc1gNLV8DIWF-zU6u6KFtJUza7H2YZabfjcQcQ=="
}

variable "sumo_source_name" {
        default = "fmt-dashboard-webapp"
}

variable "log_level" {
        default = "warn"
}

variable "public_node_env" {
        default = "production"
}

variable "public_spaces_url" {
        default = "https://findmytea.ams3.digitaloceanspaces.com"
}
