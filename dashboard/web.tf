resource "digitalocean_container_registry" "fmt_dashboard" {
  name                   = "fmt-dashboard"
  subscription_tier_slug = "starter"
}

resource "digitalocean_app" "fmt_dashboard" {
    spec {
        name    = "fmt-dashboard"
        region  = "ams"
        
        domain {
          name = "findmytea.co.uk"
          type = "PRIMARY"
          zone = "findmytea.co.uk"
        }
        domain {
          name = "www.findmytea.co.uk"
          type = "ALIAS"
          zone = "findmytea.co.uk"
        }

        
        env {
            key   = "SUMO_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.sumo_url
        }
        env {
            key   = "SUMO_SOURCE_NAME"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.sumo_source_name
        }
        env {
            key   = "LOG_LEVEL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.log_level
        }
        env {
            key   = "API_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "SECRET"
            value = var.api_key
        }
        env {
            key   = "JWT_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "SECRET"
            value = var.jwt_key
        }
        env {
            key   = "FIREBASE_PRIVATE_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "SECRET"
            value = var.firebase_private_key
        }
        env {
            key   = "ENABLED_FIREBASE"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.enabled_firebase
        }
        env {
            key   = "GOOGLE_API_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.google_api_key
        }
        env {
            key   = "EXT_API_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.ext_api_url
        }   
        env {
            key   = "USER_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.user_service_url
        }     
        env {
            key   = "SOCIAL_MEDIA_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.social_media_service_url
        }     
        env {
            key   = "LOCATION_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.location_service_url
        }    
        env {
            key   = "ESTABLISHMENT_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.establishment_service_url
        }
        env {
            key   = "CHAIN_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.chain_service_url
        }     
        env {
            key   = "LOCATION_ATTRIBUTE_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.location_attribute_service_url
        }     
        env {
            key   = "BRAND_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.brand_service_url
        }
        env {
            key   = "RESOURCE_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.resource_service_url
        }
        env {
            key   = "REFERENCE_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.reference_service_url
        }
        env {
            key   = "LAMBDA_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.lambda_service_url
        }
        env {
            key   = "ADDRESS_LOOKUP_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.address_lookup_service_url
        }
        env {
            key   = "MESSAGING_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.messaging_service_url
        }
        env {
            key   = "CLAIM_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.claim_service_url
        }
        env {
            key   = "OFFER_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.offer_service_url
        }
        env {
            key   = "SEARCH_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.search_service_url
        }
        env {
            key   = "GEOCODE_SERVICE_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.geocode_service_url
        }
        env {
            key   = "SPACES_ENDPOINT"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.spaces_endpoint
        }
        env {
            key   = "SPACES_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.spaces_key
        }
        env {
            key   = "SPACES_SECRET"
            scope = "RUN_AND_BUILD_TIME"
            type  = "SECRET"
            value = var.spaces_secret
        }

        env {
            key   = "STRIPE_PUBLISHABLE_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.stripe_key
        }
        env {
            key   = "STRIPE_SECRET_KEY"
            scope = "RUN_AND_BUILD_TIME"
            type  = "SECRET"
            value = var.stripe_secret
        }
        env {
            key   = "NEXT_PUBLIC_SUMO_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.sumo_url
        }
        env {
            key   = "NEXT_PUBLIC_SUMO_SOURCE_NAME"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.sumo_source_name
        }
        env {
            key   = "NEXT_PUBLIC_LOG_LEVEL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.log_level
        }
        env {
            key   = "NEXT_PUBLIC_NODE_ENV"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.public_node_env
        }
        env {
            key   = "NEXT_PUBLIC_SPACES_URL"
            scope = "RUN_AND_BUILD_TIME"
            type  = "GENERAL"
            value = var.public_spaces_url
        }

        # env {
        #     key   = "NODE_TLS_REJECT_UNAUTHORIZED"
        #     scope = "RUN_AND_BUILD_TIME"
        #     type  = "GENERAL"
        #     value = "0"
        # }
        
        service {
            dockerfile_path    = "Dockerfile"
            http_port          = 5000
            instance_count     = 1
            instance_size_slug = "basic-xxs"
            internal_ports     = []
            name               = "fmt-dashboard"
            source_dir         = "/"

            # health_check {
            #     initial_delay_seconds   = 120
            #     period_seconds          = 120
            # }

            image {
                registry_type = "DOCR"
                repository    = "fmt-dashboard"
                tag           = "latest"
            }

            routes {
                path = "/"
            }
        }
    } 
}