
resource "digitalocean_domain" "api2" {
  name       = "api.findmytea.co.uk"
  ip_address = digitalocean_loadbalancer.ingress.ip
}
