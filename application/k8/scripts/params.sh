
# FMT Config Map
kubectl delete configmap fmt-config

kubectl create configmap fmt-config \
  --from-literal=sumo-url=https://endpoint1.collection.eu.sumologic.com/receiver/v1/http/ZaVnC4dhaV2kw-pxmBbHS3sAX3Rv1P6lRQ3FcbX7iysdkIBYyqKTdBae5ZUkYZiKp0ms6Y573hm3bxWaYc1gNLV8DIWF-zU6u6KFtJUza7H2YZabfjcQcQ== \
  --from-literal=sumo-log-level=Warning \
  --from-literal=redis-url=redis://h:p@redis:6379 \
  --from-literal=mq-host=10.106.32.2 \
  --from-literal=mq-port=61613 \
  --from-literal=mq=activemq:tcp://10.106.32.2:61616 \
  --from-literal=mq-uri=amqps://rattlesnake.rmq.cloudamqp.com/kveojbvo \
  --from-literal=mq-username=kveojbvo \
  --from-literal=email-queue-name=fmt.email \
  --from-literal=email-to=hello@findmytea.co.uk \
  --from-literal=email-subject-prefix=[FMT] \
  --from-literal=email-default=hello@findmytea.co.uk \
  --from-literal=token-auth-issuer=FindMyTea \
  --from-literal=token-auth-audience=FindMyTeaUsers \
  --from-literal=token-auth-expire-mins=1440 \
  --from-literal=token-auth-client-key=a6a3d2ef-88e8-41fb-ac50-3d18bb891cb3 \
  --from-literal=do-spaces-key=2OQGM46UNFGXAXRQWY6I \
  --from-literal=do-spaces-service-url=https://ams3.digitaloceanspaces.com \
  --from-literal=twitter-access-token=1254818096294039553-OmjqL2DVjfCLLpajTpenn8KJUVry9k \
  --from-literal=twitter-api-key=GTG5bhsmwrRxDreukN7kN5KLs \
  --from-literal=linkedin-client-id=77q751sl9t6bxx

# Postcoder config
kubectl create configmap postcoder-config --from-literal=country-code=UK

# Google API config
kubectl create configmap geo-config --from-literal=geo-url="https://maps.googleapis.com/maps/api/" 

