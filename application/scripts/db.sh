#!/bin/bash
set -euo pipefail

# Add sudo user and grant privileges
useradd ${USERNAME} --password $(openssl passwd -1 ${PASSWORD}) --create-home --shell "/bin/bash" --groups sudo

# Create SSH directory for sudo user
mkdir --parents ${HOME_DIRECTORY}/.ssh
mkdir --parents ${HOME_DIRECTORY}/data

cp /root/.ssh/authorized_keys ${HOME_DIRECTORY}/.ssh

# Adjust SSH configuration ownership and permissions
chmod 0700 ${HOME_DIRECTORY}/.ssh
chmod 0600 ${HOME_DIRECTORY}/.ssh/authorized_keys
chown --recursive ${USERNAME}:${USERNAME} ${HOME_DIRECTORY}/.ssh

# Disable root SSH login with password
sed --in-place 's/^PermitRootLogin.*/PermitRootLogin no\
AllowUsers '${USERNAME}'/g' /etc/ssh/sshd_config
systemctl restart sshd

##########
# Docker #
##########
apt-get update
apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

usermod -aG docker ${USERNAME}

sudo chown ${USERNAME}:docker /var/run/docker.sock
service docker start

if [[ $(mount | grep /mnt/db) ]]; then
   echo '/mnt/db already mounted'
else
   mkdir -p /mnt/db
   echo '/dev/disk/by-id/scsi-0DO_Volume_devenv /mnt/db ext4 defaults,nofail,discard 0 0' | sudo tee -a /etc/fstab
   mount /mnt/db
fi

mkdir -p /mnt/db/mongo
mkdir -p /mnt/db/redis

chown --recursive ${USERNAME}:docker /mnt/db/


#############
# Mongo #
#############

docker run -d --restart always --name mongo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=findmytea -e MONGO_INITDB_ROOT_PASSWORD=${MONGO_PASSWORD} -v /mnt/db/mongo:/data/db mongo

# docker run -d --restart always --name redis -p 6379:6379 redis

# Logs: sudo tail -f /var/log/cloud-init-output.log
