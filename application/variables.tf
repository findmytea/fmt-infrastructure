
variable "do_token" {
    description = "Digital ocean token"
}

data "digitalocean_ssh_key" "ssh_key" {
  name = "Paul"
}

variable "password" {
    description = "findmytea user password"
}

variable "mongo_password" {
    description = "mongo password"
}

data "digitalocean_kubernetes_versions" "fmt_k8" {
  version_prefix = "1.19"
}

variable "sumo_url" {
    default = "https://endpoint1.collection.eu.sumologic.com/receiver/v1/http/ZaVnC4dhaV2kw-pxmBbHS3sAX3Rv1P6lRQ3FcbX7iysdkIBYyqKTdBae5ZUkYZiKp0ms6Y573hm3bxWaYc1gNLV8DIWF-zU6u6KFtJUza7H2YZabfjcQcQ=="
}

variable "sumo_source_name" {
    default = "fmt-dashboard"
}

variable "log_level" {
    default = "information"
}


