
output "kubernetes-id" {
  value = digitalocean_kubernetes_cluster.k8.id
}

output "kubernetes-save" {
  value = "doctl kubernetes cluster kubeconfig save "
}

output "kubernetes-cmd" {
  value = "kubectl --context do-lon1-fmt-k8"
}

output "password" {
    value = var.password
}
