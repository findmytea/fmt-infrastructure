
resource "digitalocean_kubernetes_cluster" "k8" {
  name          = "fmt-k8"
  region        = "lon1"
  auto_upgrade  = true
  vpc_uuid      = digitalocean_vpc.vpc.id
  version       = data.digitalocean_kubernetes_versions.fmt_k8.latest_version

  node_pool {
    name       = "autoscale-worker-pool"
    size       = "s-1vcpu-2gb"
    auto_scale = true
    min_nodes  = 4
    max_nodes  = 4
  }
}

resource "digitalocean_loadbalancer" "ingress" {
  name                     = "a6e289c4b90f146599fce1908525d661"
  redirect_http_to_https   = false
  region                   = "lon1"
  size                     = "lb-small"

  forwarding_rule {
        entry_port      = 443
        entry_protocol  = "tcp"
        target_port     = 30203
        target_protocol = "tcp"
        tls_passthrough = false
    }
    forwarding_rule {
        entry_port      = 80
        entry_protocol  = "tcp"
        target_port     = 31918
        target_protocol = "tcp"
        tls_passthrough = false
    }

    healthcheck {
        check_interval_seconds   = 3
        healthy_threshold        = 5
        port                     = 31918
        protocol                 = "tcp"
        response_timeout_seconds = 5
        unhealthy_threshold      = 3
    }

}