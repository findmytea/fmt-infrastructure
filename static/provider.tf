
# Configure the DigitalOcean Provider
provider "digitalocean" {
  token   = var.do_token
  version = "~> 1.22"
}

terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "Paul_Grenyer"

    workspaces {
      name = "fmt-static"
    }
  }
}
