resource "digitalocean_volume" "fmt" {
  region                  = "lon1"
  name                    = "fmt"
  size                    = 1
  initial_filesystem_type = "ext4"
  description             = "storage for find my tea"
}

// Add Find My Tea Spaces

resource "digitalocean_floating_ip" "fmt" {
  region     = "lon1"
}

resource "digitalocean_record" "fmt_api" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "api"
  value  = digitalocean_floating_ip.fmt.ip_address
}

resource "digitalocean_record" "fmt_mq" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "mq"
  value  = digitalocean_floating_ip.fmt.ip_address
}

resource "digitalocean_record" "fmt_dashboard" {
  domain = data.digitalocean_domain.devenv.name
  type   = "A"
  name   = "dashboard"
  value  = digitalocean_floating_ip.fmt.ip_address
}