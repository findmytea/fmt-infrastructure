
variable "do_token" {
    description = "Digital ocean token"
}

data "digitalocean_domain" "devenv" {
  name = "findmytea.co.uk"
}
